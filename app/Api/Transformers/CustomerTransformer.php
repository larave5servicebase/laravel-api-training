<?php

namespace Api\Transformers;

use App\Customer;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
	public function transform(Customer $customer)
	{
		return [
			'id' 	=> (int) $customer->id,
			'name'  => $customer->name,
			'age'	=> (int) $customer->age
		];
	}
}
