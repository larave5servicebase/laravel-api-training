<?php

namespace Api\Controllers;

use Api\Requests\CustomerRequest;
use App\Customer;
use App\Http\Requests;
use Illuminate\Http\Request;
use Api\Transformers\CustomerTransformer;
use Dingo\Api\Facade\API;

/**
 * @Resource('Customers', uri='/customers')
 */
class CustomersController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Show all customers
     *
     * Get a JSON representation of all the customers
     *
     * @Get('/')
     */
    public function index()
    {
        $customers =  $this->collection(Customer::all(), new CustomerTransformer);
        if ($customers){
            return API::response()->array(['status' => 'success', "customers" => $customers])->statusCode(200);
        }
    }

    /**
     * @param CustomerRequest $request
     * @return static
     */
    public function store(CustomerRequest $request)
    {
        $customer = Customer::create($request->only(['name', 'age']));
        if ($customer){
            return API::response()->array(['status' => 'success', "customer" => $customer])->statusCode(200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = $this->item(Customer::findOrFail($id), new CustomerTransformer);

        if ($customer){
            return API::response()->array(['status' => 'success', "customer" => $customer])->statusCode(200);
        }
    }

    /**
     * @param CustomerRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CustomerRequest $request, $id)
    {
        $customer = Customer::findOrFail($id);

        $customer->update($request->only(['name', 'age']));

        if ($customer){
            return API::response()->array(['status' => 'success', "customer" => $customer])->statusCode(200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Customer::destroy($id)){
            return API::response()->array(['status' => 'success'])->statusCode(200);
        }
    }
}
