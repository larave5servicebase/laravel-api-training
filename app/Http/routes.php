<?php
$api = app('Dingo\Api\Routing\Router');

Route::get('/', function () {
        return view('spa');
    });

// Version 1 of our API
$api->version('v1', function ($api) {

	// Set our namespace for the underlying routes
	$api->group(['namespace' => 'Api\Controllers', 'middleware' => '\Barryvdh\Cors\HandleCors::class'], function ($api) {

		// Login route
		$api->post('login', 'AuthController@authenticate');
		$api->post('register', 'AuthController@register');

		// Dogs! All routes in here are protected and thus need a valid token
		//$api->group( [ 'protected' => true, 'middleware' => 'jwt.refresh' ], function ($api) {
		$api->group( [ 'middleware' => 'jwt.auth' ], function ($api) {

			$api->get('users/me', 'AuthController@me');
			$api->get('validate_token', 'AuthController@validateToken');

			$api->get('customers', 'CustomersController@index');
            $api->post('customers', 'CustomersController@store');
            $api->get('customers/{id}', 'CustomersController@show');
            $api->delete('customers/{id}', 'CustomersController@destroy');
            $api->post('customers/{id}', 'CustomersController@update');

		});

	});

});
